AI Library
=========

The role will deploy the AI Library models via ```SeldonDeployments```.

Dependencies
------------

Seldon must be installed and running in the same namespace the models are being deployed.

The installation can be done manually or via the Open Data Hub operator.

Role Variables
--------------

These are all optional, but can be set to allow you to have easier access to S3 storage
from inside your predictor containers.

* `s3_endpoint` - Sets the S3_ENDPOINT_URL environment variable in predictor containers
* `s3_access` - Sets the S3_ACCESS_KEY environment variable in predictor containers
* `s3_secret` - Sets the S3_SECRET_KEY environment variable in predictor containers
* `s3_bucket` - Sets the S3_BUCKET environment variable in predictor containers
* `s3_region` - Sets the S3_REGION environment variable in predictor containers

Example Variable Spec
---------------------

```
ai-library:
  odh_deploy: true
  s3_endpoint: "http://s3.foo.com:8000"
  s3_access: "YOURS3ACCESSKEYHERE"
  s3_secret: "this1is2just3gibberish"
  s3_bucket: "thisismybucketname"
  s3_region: "myregion"
```

License
-------

GNU GPLv3

Author Information
------------------

croberts@redhat.com
